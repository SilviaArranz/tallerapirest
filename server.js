var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  var path = require ('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/Clientes', function(req,res){
  res.sendFile(path.join(__dirname, 'get.json'));
})

app.post('/Clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'post.json'));
})

app.put('/Clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'put.json'));
})

app.delete('/Clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'delete.json'));
})
app.get('/Clientes/:idCliente', function(req, res){
  res.sendFile(path.join(__dirname, req.params.idCliente + '.json'));
})
//API de Express, funciones GET y POST. ("ruta", function)
app.get('/', function(req, res){
res.sendFile(path.join(__dirname, 'index.html'));
});
